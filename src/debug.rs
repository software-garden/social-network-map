use bevy::prelude::*;
use bevy_inspector_egui::WorldInspectorPlugin;

pub struct DebugPlugin;

impl Plugin for DebugPlugin {
    fn build(&self, app: &mut App) {
        if cfg!(debug_assertions) {
            app.add_plugin(WorldInspectorPlugin::new());
            app.insert_resource(bevy::log::LogSettings {
                level: bevy::log::Level::DEBUG,
                ..default()
            });
        } else {
            app.insert_resource(bevy::log::LogSettings {
                level: bevy::log::Level::INFO,
                ..default()
            });
        }
    }
}
