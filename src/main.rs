use bevy::{prelude::*, sprite::MaterialMesh2dBundle};
use bevy_mod_picking::*;
use bevy_pancam::{PanCam, PanCamPlugin};
use markdown;
use std::fs;
mod debug;

fn main() {
    App::new()
        .insert_resource(WindowDescriptor {
            title: "Social Network Map".to_string(),
            ..default()
        })
        .add_plugins(DefaultPlugins)
        .add_plugins(DefaultPickingPlugins)
        .add_plugin(PanCamPlugin)
        .add_plugin(debug::DebugPlugin)
        .add_startup_system(setup)
        .add_system(bevy::input::system::exit_on_esc_system)
        .run();
}

fn setup(
    mut commands: Commands,
    mut meshes: ResMut<Assets<Mesh>>,
    mut materials: ResMut<Assets<ColorMaterial>>,
    assets: Res<AssetServer>,
) {
    let people = read_markdown();
    let nodes = people.iter().enumerate().map(|(position, person)| -> Node {
        Node {
            priority: position,
            label: person.to_string(),
        }
    });

    let font_upscale = 10.0;
    let font = assets.load("Cantarell-ExtraBold.otf");
    let text_style = TextStyle {
        font,
        color: Color::WHITE,
        font_size: 20. * font_upscale,
    };
    let text_alignment = TextAlignment {
        horizontal: HorizontalAlign::Left,
        vertical: VerticalAlign::Top,
    };

    commands
        .spawn_bundle(OrthographicCameraBundle::new_2d())
        .insert(PanCam {
            grab_buttons: vec![MouseButton::Middle],
            enabled: true,
        })
        .insert_bundle(PickingCameraBundle::default());

    let size = Size {
        width: 164.,
        height: 96.,
    };

    let gap = 32.;
    let padding = 12.;

    for node in nodes {
        let translation = Vec3::new(0., -(size.height + gap) * (node.priority as f32), 0.);

        commands
            .spawn_bundle(MaterialMesh2dBundle {
                mesh: meshes
                    .add(Mesh::from(shape::Quad {
                        size: Vec2::new(size.width, size.height),
                        ..default()
                    }))
                    .into(),
                transform: Transform {
                    translation,
                    scale: Vec3::splat(1.),
                    rotation: Quat::default(),
                },
                material: materials.add(ColorMaterial::from(Color::ORANGE)),
                ..Default::default()
            })
            .insert(Name::from("Box"))
            .insert_bundle(PickableBundle::default())
            .insert(node.clone());

        commands.spawn_bundle(Text2dBundle {
            text: Text::with_section(node.label, text_style.clone(), text_alignment),
            transform: Transform {
                translation: translation
                    + Vec3::new(-size.width / 2. + padding, size.height / 2. - padding, 1.),
                rotation: Quat::default(),
                scale: Vec3::splat(1.0 / font_upscale),
            },
            ..default()
        });
    }
}

fn read_markdown() -> Vec<String> {
    let markdown = fs::read_to_string("samples/sample.md").unwrap();
    let document = markdown::tokenize(&markdown);
    let headers: Vec<String> = document
        .into_iter()
        .filter_map(|block| {
            if let markdown::Block::Header(spans, 1) = block {
                let text = spans_to_text(&spans);
                return Some(text);
            };
            None
        })
        .collect();
    headers
}

fn spans_to_text(spans: &[markdown::Span]) -> String {
    spans.iter().map(span_to_text).collect()
}

fn span_to_text(span: &markdown::Span) -> String {
    match span {
        markdown::Span::Break => "\n".to_string(),
        markdown::Span::Text(text) => text.clone(),
        markdown::Span::Code(code) => code.clone(),
        markdown::Span::Link(_href, label, _titlee) => label.clone(),
        markdown::Span::Image(_src, alt, _title) => alt.clone(),
        markdown::Span::Emphasis(spans) => spans_to_text(spans),
        markdown::Span::Strong(spans) => spans_to_text(spans),
    }
}

#[derive(Component, Clone)]
struct Node {
    priority: usize,
    label: String,
}
